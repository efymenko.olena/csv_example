"""Завдання 3. Попрацюйте зі створенням власних діалектів, вибираючи правила для CSV файлів.
Зареєструйте створені діалекти та попрацюйте, використовуючи їх зі створенням/читанням файлом.
"""
import csv

class CustomDialect(csv.Dialect):
    quoting = csv.QUOTE_ALL
    quotechar = "~"
    delimiter = " "
    lineterminator = '\n'

csv.register_dialect("my_dialect", CustomDialect)

with open ("output.csv", "w") as f:
    writer = csv.writer(f, dialect="my_dialect")
    writer.writerow(["country", "populated_locality", "population", "main_language"])
    writer.writerow(["Great Britan", "London", "8 961 989", "english"])
    writer.writerow(["Poland", "Krakow", "766 683", "polish"])
    writer.writerow(["Ukraine", "Odessa", "993 120", "ukrainian"])
    writer.writerow(["Lithuania", "Vilnius", "544 386", "lithuanian"])
    writer.writerow(["Czech Republic", "Prague", "1 275 406", "сzech"])

#due to Sniffer we can parse file and correctly decrypt the data
sniffer = csv.Sniffer
dialect = "my_dialect"

with open("output.csv", "r") as f:
    reader = csv.reader(f, dialect=dialect)
    for row in reader:
        print(row)

